package
{
	import flash.text.TextField;

	public class Board
	{
		public var cells:Array =  new Array();
		public var queue:Array =  new Array();
		public var words:Array =  new Array();
		private var result:String ;
		private var index:int ;
		public function Board()
		{
			var i:int;
			for ( i = 0; i < 4; i++)
			{
			cells[i] = new Array();
			}
		}

		
		public function clear_board():void
		{
			var i:int;
			var j:int;
			for ( i = 0; i < 4; i++)
			{
				for( j =0 ; j<4;j++)
				{
					cells[i][j] = null;
				}
			}
			trace( "Clear the board");
		}

		
		public function fill_board():void
		{
			trace("Matrix filled");
			var i:int;
			var j:int;
			var strarray:Array = getRandomStringArray(16);
			
			for ( i = 0; i < 4; i++)
			{
				for( j =0 ; j<4;j++)
				{
					cells[i][j] = new Cell(strarray[j+i*4]) ;
				}
			}
		
		}
		
		private function getRandomStringArray(len:int):Array
		{
			// TODO Auto Generated method stub
			var newStr:String = "";
			while(newStr.length < 16)
			{
				newStr += Dictionary.words[Math.floor( Math.random() * 234936) % (Dictionary.words.length)];
			}
			
			trace(newStr);
			var strArr:Array =newStr.toUpperCase().split("");
			var i:int;
			for(i = 0 ; i< 16;i++)
			{
				var rand:int = Math.floor( Math.random() *1000)%16;
				var temp:String =  strArr[rand]  ;
				strArr[rand]   = strArr[15-rand]  ;
				strArr[15-rand]=temp;  
			}
			
			return strArr;
		}
		
		public function letterPresses(i:int, j:int):void
		{
			var pos:int = i * 4 + j;
			var p:int = queue.indexOf(pos);
			if (  queue.indexOf(pos) >= 0)
			{
				trace("Already Present");
			}else
			{
				queue[index++] = pos;
			}
			
		}
		
		public function getResult():String
		{
			var result:String  = "";
			for(var i:String in queue){
				var x:int  = queue[i] / 4
				var y:int = queue[i] % 4;
				//result = cells[x][y].value +result + "" ;
				result = result +cells[x][y].value  ;
			}
			queue = new Array();
			
			index = 0;
			return result;
			
		}
		
		public function isValid(word:String):Boolean
		{
			if (words.indexOf(word) >=0)
			{
				return false;
			}else
			{
			words[words.length] = word;
			return true;
			}
			
		}
		

		

	}	
	
	
}