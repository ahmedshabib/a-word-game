package
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.sampler.NewObjectSample;
	import flash.system.System;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.utils.setTimeout;
	
	import flash.display.BitmapData;
	import flash.display.Bitmap;

	public class ScoreView extends Sprite
	{
		public var ScoreText:TextField ;
		public var tf:TextFormat
		public var backButton:Sprite;
		
		[Embed(source = "../Assets/back.jpg")]
		private var BackButtonClass:Class;
		
		public function ScoreView(score:int)
		{
			ScoreText =  new TextField();
			ScoreText.text = "Your Score is\n" + score ;
			ScoreText.width = 400;
			ScoreText.height = 300;
			ScoreText.x = 40;
			ScoreText.y = 200;
			tf = new TextFormat();
            tf.size = 70;
            tf.color = 0x96777F;
			tf.align = "center";
            ScoreText.setTextFormat(tf);
			this.addChild(ScoreText);
			
			addBackButton();
		}
		
		private function addBackButton():void 
		{
			backButton = new Sprite();
			backButton.addChild(new BackButtonClass);
			backButton.x = 0;
			backButton.y = 0;
			backButton.width = 100;
			backButton.height = 100;
			this.addChild(backButton);
			
			backButton.addEventListener(MouseEvent.CLICK, goBackEvent);
		}
		
		private function goBackEvent(e:MouseEvent):void 
		{
			
			this.addChild(new Game());
		}
	}
}