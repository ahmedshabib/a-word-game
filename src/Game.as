package
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.utils.clearTimeout;
	import flash.utils.setTimeout;
	import flash.utils.Timer;
	import flash.text.TextField;
	import flash.text.TextFormat;	
	import flash.display.BitmapData;
	import flash.display.Bitmap;
	public class Game extends Sprite
	{
		public var player:Player;
		private var currentScore:int;
		private var timeout:uint;
		public var mainScreen:Sprite;
		public var boardView:BoardView ;
		

		public var timer:Timer;
		[Embed(source = "../Assets/Logo.jpg")]
		private var LogoClass:Class;
		[Embed(source = "../Assets/MainScreen.jpg")]
		private var MainScreenClass:Class;
		public var d:Dictionary;
		public function Game()
		{
			trace("Game Created");
			player = new Player("Player1");
			var d:Dictionary = new Dictionary();
			loadMainScreen();
		}
		
		private function loadMainScreen():void
		{
			// TODO Auto Generated method stub
			

			mainScreen = new Sprite();
			mainScreen.addChild(new MainScreenClass());
			this.addChild(mainScreen);
			mainScreen.addEventListener(MouseEvent.CLICK,doMainScreenAction);
			
			var mainLogoSprite:Sprite = new Sprite();
			mainLogoSprite.addChild(new LogoClass());
			mainLogoSprite.x = 60;
			mainLogoSprite.y = 50;
			mainScreen.addChild(mainLogoSprite);
		}
		
		protected function doMainScreenAction(event:MouseEvent):void
		{
			// TODO Auto-generated method stub
			if(event.localX > 100 && 	event.localX <400 && event.localY > 300 && event.localY < 375)
			{
				start_game();
			}
		}
		

		
		public function start_game():void
		{
			this.removeChild(mainScreen);
			boardView = new BoardView();
			this.addChild(boardView);
			boardView.backButton.addEventListener(MouseEvent.CLICK, goBackEvent);
			start_timer(60000);
		}
		
		private function goBackEvent(e:MouseEvent):void 
		{
			this.removeChild(boardView);
			stop_timer();
			this.addChild(mainScreen);
		}
		public function time_up(val:String):void
		{
			stop_timer();
			trace("Game is over");
			save_score();
			this.removeChild(boardView);
			navigate_score_page(boardView.getScore());
		}
		
		private function navigate_score_page(score:int):void
		{
			// TODO 
			//navigate to the score page
			
			this.addChild(new ScoreView(score));
		}
		
		private function save_score():void
		{
			// TODO 
			//Save the score into persistence
			
		}
		
		public function start_timer(t : int):void
		{
			timeout = setTimeout(time_up,t,"");
			timer = new Timer(1000);
			timer.start();
			timer.addEventListener(TimerEvent.TIMER, timerTickHandler);
			var timerCount:int = 0;
		}
		
		private function timerTickHandler(e:TimerEvent):void 
		{
			trace(timer.currentCount);
			boardView.timerField.text = "Time Left : " +( 60 - timer.currentCount) +"";
			
			var tf:TextFormat = new TextFormat();
            tf.size = 25;
            tf.color = 0xFF0000;
            boardView.timerField.setTextFormat(tf);
			
		}
		
		
		public function stop_timer():void
		{
			clearTimeout(timeout);
			timer.removeEventListener(TimerEvent.TIMER, timerTickHandler);
		}
	}
}