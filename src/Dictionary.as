package
{
	import flash.events.Event;
	import flash.net.FileReference;
	import flash.net.URLLoader;
	import flash.net.URLRequest;

	public class Dictionary
	{
		public static  var words:Array =  new Array();
		public function Dictionary()
		{
			var myTextLoader:URLLoader = new URLLoader();
			myTextLoader.addEventListener(Event.COMPLETE, onLoaded);
			myTextLoader.load(new URLRequest("words.txt"));
		}		
		
		public function onLoaded(e:Event):void {
			words = e.target.data.toLowerCase().split("\r").join("").split(/\n/);
			trace("I am called");
		}
		
		public static function is_word(word:String):Boolean
		{	trace(word);
			if (words.indexOf(word.toLowerCase()) >= 0 && word.length >=3) 
			{
				return true;
			}
			return false;
		}
		
		public static function get_points(word:String):int
		{ 
			var i:int;
			var wordarray:Array = word.split("");
			for(i=0; i <wordarray.length ; i++)
			{
				//trace(wordarray[i]);
			}
		 	if(word.length == 3)
				return 3;
			else if(word.length ==4)
				return 5;
			else if(word.length == 5)
				return 7;
			else if(word.length == 6)
				return 9;
			else if(word.length == 7)
				return 13;
			else if(word.length == 8)
				return 18;
			else if(word.length == 9)
				return 24;
			else if(word.length == 10)
				return 30
			else if(word.length == 11)
				return 40;
			else if(word.length == 12)
				return 50;
			else if(word.length == 13)
				return 60;
			else if(word.length == 14)
				return 70;
			else if(word.length == 15)
				return 80;
			else if(word.length == 16)
				return 100;
			else return 0;
		}
	}
}