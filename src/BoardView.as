package
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.sampler.NewObjectSample;
	import flash.system.System;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.utils.setTimeout;
	import flash.text.TextFormat;
	import flash.display.BitmapData;
	import flash.display.Bitmap;

	public class BoardView extends Sprite
	{
		public var cells:Array =  new Array();
		public var cellsText:Array =  new Array();
		public var grid:Sprite;
		public var backButton:Sprite;
		public var board:Board = new Board();
		public var result:String;
		private var currentScore:int;
		public var timerField:TextField ;
		public var scoreField:TextField ;
		public var tfGrid:TextFormat;
		public var tfSelected:TextFormat;
		public var tfCorrect:TextFormat;
		public var tfWrong:TextFormat;
		public var tfTimer:TextFormat;
		public var tfScore:TextFormat;
		
		[Embed(source = "../Assets/grid.jpg")]
		private var GridClass:Class;

		
		[Embed(source = "../Assets/back.jpg")]
		private var BackButtonClass:Class;

		public function BoardView()
		{
			//Add The Grid image to the Game Screen
			declareTextFormats();
			addGrid(); 
			board.clear_board();
			board.fill_board();
			fillGrid();
			addScoreBoard();
			addBackButton();
			addTimerToBoardView();
			addScoreToBoardView();
		}
		
		
		private function declareTextFormats():void 
		{
			tfTimer = new TextFormat();
            tfTimer.size = 25;
            tfTimer.color = 0xFF0000;
			
			tfScore = new TextFormat();
            tfScore.size = 25;
            tfScore.color = 0x4545FF;
			
			tfGrid = new TextFormat();
            tfGrid.size = 25;
			tfGrid.align = "center";
			
			tfSelected = new TextFormat();
            tfSelected.size = 25;
			tfSelected.align = "center";
		}
		
		public function getScore():int 
		{
			return currentScore;
		}
		private function addTimerToBoardView():void 
		{
			timerField =  new TextField();
			timerField.height = 40;
			timerField.width = 200;
			timerField.text = "Time Left : 60" ;
			timerField.x = 320;
			timerField.y = 10;

            timerField.setTextFormat(tfTimer);
			this.addChild(timerField);
		}
		
		private function addScoreToBoardView():void 
		{
			scoreField =  new TextField();
			scoreField.height = 40;
			scoreField.width = 200;
			scoreField.text = "Your Score : 0" ;
			scoreField.x = 320;
			scoreField.y = 40;
			

            scoreField.setTextFormat(tfScore);
			this.addChild(scoreField);
		}
		
		private function updateScore():void 
		{
			scoreField.text = "Your Score : " + currentScore ;
			scoreField.setTextFormat(tfScore);
		}
		private function addBackButton():void 
		{
			backButton = new Sprite();
			backButton.addChild(new BackButtonClass);
			backButton.x = 0;
			backButton.y = 0;
			backButton.width = 100;
			backButton.height = 100;
			this.addChild(backButton);
			

		}
		

		
		private function addScoreBoard():void 
		{
			
		}
		
		private function fillGrid():void
		{
			// TODO Auto Generated method stub
			var i:int;
			var j:int;
			for(i = 0 ; i < 4 ; i++){
				cellsText[i] = new Array();
				cells[i] = new Array();
				for(j = 0 ; j < 4 ; j++){
					//Initializing the Grid with the Characters
					fillInitialGrid(i,j);
					//This is for the selected Cell Will be invisible in beginning.
					fillSelectedCell(i,j);
				}
			}
			

		}
		
		private function addGrid():void
		{
			// TODO This will generate a Grid Sprite and add it to the game screen
			grid = new Sprite();
			grid.addChild(new GridClass());
			grid.y = 150;
			grid.x = 50;
			
			this.addChild(grid);
			
			grid.addEventListener(MouseEvent.MOUSE_DOWN, startTracking);
			grid.addEventListener(MouseEvent.MOUSE_UP, stopTracking);
		}
		

		
		private function fillInitialGrid(i:int, j:int):void
		{
			// TODO This will fill the grid with intial set of values from the "board" object .
			cellsText[i][j]  = new Sprite();
			var textField:TextField =  new TextField();
			textField.selectable = false;
			textField.height = 30;
			textField.width = 30;

			textField.text = board.cells[i][j].value;

            textField.setTextFormat(tfGrid);
			var myTextImage:BitmapData = new BitmapData(textField.width, textField.height, true, 0x000000ff);
			myTextImage.draw(textField);
			cellsText[i][j].addChild(new Bitmap(myTextImage));
			cellsText[i][j].y = 185 + 100*j;
			cellsText[i][j].x = 85 + 100*i;
			this.addChild(cellsText[i][j]);
			
			cellsText[i][j].addEventListener(MouseEvent.MOUSE_DOWN,startTracking);
			
		}
		
		private function fillSelectedCell(i:int, j:int):void
		{
			// TODO This will add invisible cells with intial set of values from the "board" object . Will become visible when clicked.
			var textf2:TextField =  new TextField();
			
			textf2.selectable = false;
			textf2.height = 98;
			textf2.width = 98;
			textf2.text =  "\n"+board.cells[i][j].value;
	
			textf2.background = true;
			textf2.backgroundColor = 0xF0474E;
            textf2.setTextFormat(tfSelected);
			cells[i][j]  = new Sprite();
			cells[i][j].addChild(textf2);
			cells[i][j].y = 151+ 100*j;
			cells[i][j].x = 51 +100*i;
			this.addChild(cells[i][j]);
			cells[i][j].visible =false;
			//When mouse UP on the cell clicked .
			cells[i][j].addEventListener(MouseEvent.MOUSE_UP, stopTracking);
			
		}

		protected function stopTracking(event:MouseEvent):void
		{
			// TODO Auto-generated method stub
			trace("I am removed");
			grid.removeEventListener(MouseEvent.MOUSE_MOVE,cellClicked);
			result = board.getResult();
			trace(result);
			if(Dictionary.is_word(result)  && board.isValid(result)){
				currentScore += Dictionary.get_points(result) ;
				updateScore();
				//Correct Answer
				changeColor(0x388C0A);
			}else if(Dictionary.is_word(result))
			{
				//Already present
				changeColor(0xF9EB4E);
			}else
			{
				//Wrong answer
				changeColor(0xFF0000);
			}
			
			
			setTimeout(clearClicked,250);
		}
		
		private function changeColor(val:int):void
		{
			trace("Screen Cleared");
			// TODO Auto Generated method stub
			var i:int;
			var j:int;
			for(i = 0 ; i < 4 ; i++){
				for(j = 0 ; j < 4 ; j++){
					if(board.cells[i][j].selected){
					var textf2:TextField = cells[i][j].getChildAt(0);
					textf2.background = true;
					textf2.backgroundColor = val;
					textf2.setTextFormat(tfSelected);
					
					board.cells[i][j].selected = false;
					}
				}
			}
		}

		
		private function clearClicked():void
		{
			trace("Screen Cleared");
			// TODO Auto Generated method stub
			var i:int;
			var j:int;
			for(i = 0 ; i < 4 ; i++){
				for (j = 0 ; j < 4 ; j++) {
					cells[i][j].visible = false;
					var textf2:TextField = cells[i][j].getChildAt(0);
					textf2.background = true;
					textf2.backgroundColor = 0xF0474E;
					textf2.setTextFormat(tfSelected);
						
					
				}
			}
		}
		
		protected function startTracking(event:MouseEvent):void
		{
			// TODO Auto-generated method stub
			trace("I Just Clicked");
			result = "";
			grid.addEventListener(MouseEvent.MOUSE_MOVE, cellClicked);
			var x:int = (event.stageX-50) /101;
			var y:int = (event.stageY -150) / 101;
			trace(x + "   "+ y);
			cells[x][y].visible = true;
			board.letterPresses(x, y);
			board.cells[x][y].selected = true;
		}
		
		protected function cellClicked(event:MouseEvent):void
		{
			// TODO Auto-generated method stub
			if ( event.stageX > 50  && event.stageX < 450 && event.stageY > 150 && event.stageY < 550)
			{
				var x:int = event.localX /101;
				var y:int = event.localY /101;
				//Dont consider Edge cases
				if ( (event.localX / 101.0 - Math.floor(event.localX / 101.0) ) > 0.2 && (event.localX / 101.0 - Math.floor(event.localX / 101.0) ) < 0.8 )
				{
					trace(x + "   "+ y);
					cells[x][y].visible = true;
					
					board.letterPresses(x, y);
				}
				board.cells[x][y].selected = true;
			}else
			{
				stopTracking(event);
			}
		}
	}
}